package com.example.admin.roomdatabasedemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ListView;

import com.example.admin.roomdatabasedemo.AppDatabase.AppDatabase;
import com.example.admin.roomdatabasedemo.dao.UserDao;
import com.example.admin.roomdatabasedemo.entity.User;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button btnSave;
    ListView listView;
    List<String> userlist;
    User user;
    AppDatabase appDatabase;
    UserDao dao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnSave = findViewById(R.id.btnSave);
        listView = findViewById(R.id.lvResult);

        user = new User();
        user.setuName("Dalay");

       dao= AppDatabase.getINSTANCE(this).userDao();
       // dao = appDatabase.userDao();
        dao.insertUser(user);
     //   Log.e("data=>", " mydata"+dao.getUser());


    }
}
