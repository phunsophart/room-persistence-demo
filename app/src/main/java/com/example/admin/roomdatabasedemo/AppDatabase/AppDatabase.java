package com.example.admin.roomdatabasedemo.AppDatabase;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.example.admin.roomdatabasedemo.dao.UserDao;
import com.example.admin.roomdatabasedemo.entity.User;

@Database(entities = {User.class},version = 1)
public abstract class AppDatabase extends RoomDatabase {

    //static String mydb="user_db";
    public abstract UserDao userDao();
   // public static AppDatabase INSTANCE;

   public static  AppDatabase getINSTANCE(Context context){
       return Room.databaseBuilder(context,AppDatabase.class,"my-db")
               .allowMainThreadQueries()
               .build();
    }


}
